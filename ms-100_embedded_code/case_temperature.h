//===============================================================================================================================================
/*
Project name: MS-100
File name: case_temperature.h
Description: 
*/
//===============================================================================================================================================

#ifndef CASE_TEMPERATURE_H
#define CASE_TEMPERATURE_H

static constexpr unsigned long wto_stack_size{3048};
static constexpr unsigned int wto_task_system_status_bit{0x02};

static constexpr unsigned long restart_interval{500UL}; // 5 seconds

static constexpr TickType_t wto_task_delay_ms{pdMS_TO_TICKS(1000UL)};

static constexpr int temp_sensor{GPIO_NUM_27}; // System's temperature sensor for the solenoid
static constexpr int fan_pcb{GPIO_NUM_12};  // FAN PCB Control  

// static constexpr int reset_button{GPIO_NUM_4};  // Reset button

extern BaseType_t xWTOTaskStatus;
extern TaskHandle_t xWTOTaskHandle;
extern QueueHandle_t xCaseTemperatureQueue;

void caseTemperatureTask(void *wto_params);

#endif