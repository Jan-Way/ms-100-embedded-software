//===============================================================================================================================================
/*
Project name: MS-100
File name: WiFiManager.h
Description: 
*/
//===============================================================================================================================================

#ifndef WIFI_MANAGER_H
#define WIFI_MANAGER_H

static constexpr unsigned long wifi_stack_size{8192};

//static constexpr unsigned int wifi_task_switch_network_bit{0x08}; // To notify WiFi task to switch the network

static constexpr TickType_t wifi_task_delay_ms{pdMS_TO_TICKS(1UL)};
static constexpr TickType_t wait_connection_time{pdMS_TO_TICKS(1000UL)};
static constexpr unsigned long reconnect_interval{5000UL};     // 5 seconds

static const char* ssid = "INFINITUM8AA4_2.4";
static const char* password = "38544v9kHf";

// RTOS task handles and status
extern BaseType_t xWiFiTaskStatus;
extern TaskHandle_t xWiFiTaskHandle;
extern QueueHandle_t xWiFiQueue;

void wifiTask(void *wifi_params);

#endif