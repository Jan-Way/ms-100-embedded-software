//===============================================================================================================================================
/*
Project name: MS-100
File name: battery.cpp
Description: 
*/
//===============================================================================================================================================

#include "Arduino.h"
#include "battery.h"
#include "ms_types.h"
#include "esp32-hal-adc.h"  // needed for adc pin reset
#include "soc/sens_reg.h"   // needed for adc pin reset

BaseType_t xBatteryTaskStatus;
TaskHandle_t xBatteryTaskHandle;
QueueHandle_t xBatteryQueue;

static unsigned long battery_check_ref{0};  // Timer reference for the battery routine counter

//bool charging_state = false;

// Backup analog pin register
//uint64_t backup_pin_register_battery; // Used to store the analog pin register

/* 
  Description: Function to read the battery voltage, process it and give an output in percentage.
  At 3.3V power supply, the values will be 0-16.5VDC = 0-2048 with a resolution of 11 bits (0.001612115VDC per unit). 

  Note: Based on the batteries datasheet, the max output voltage are 13.6VDC = 100% charged, and minimun is 10.0VDC = 0% totally discharged.
  Furthermore, we need to consider that the real 20% of the battery level, for the user we need to show 0%, since we need to leave some battery life
  backup for security, so the real 20-100% needs to be scaled to 0-100%, to set this reference as the minimun charge voltage. So in real terms of the
  Mop system requirements, let's consider that 13.5VDC = 100% charged, and 12.9VDC = 0% (discharged with backup included).

  Useful information (11 bits):
    - 13.6VDC = 1687 ADC Value = 100% charge
    - 12.9VDC = 1600 ADC Value = 0% charge

  Useful information (12 bits):
    - 13.6VDC = 3201 ADC Value = 100% charge
    - 12.9VDC = 3375 ADC Value = 0% charge

    12V LiFePO4 Battery

    State-Of-Charge 	Voltage at rest (zero current) 	Voltage under load (0.25C)
        100% 	                    14.0 Volt 	                13.6 Volt
        99% 	                    13.8 Volt 	                13.4 Volt
        90% 	                    13.4 Volt 	                13.3 Volt
        70% 	                    13.2 Volt 	                13.2 Volt
        40% 	                    13.2 Volt 	                13.1 Volt
        30% 	                    13.0 Volt 	                13.0 Volt
        20% 	                    12.9 Volt 	                12.9 Volt
        17% 	                    12.8 Volt 	                12.8 Volt
        14% 	                    12.6 Volt 	                12.5 Volt
        9% 	                      12.4 Volt 	                12.0 Volt
        0% 	                      10.4 Volt 	                10.0 Volt
*/

float readAverageValue(){
  float values;
  for(int i=0; i<20; i++){
    values = values + analogRead(voltage_sensor);
  }
  return values / 20;
}

float readBatteryLevel()
{
    // Returns the result conversion of the value read from the ADC pin associated to the battery voltage, to a useful percentage value
    unsigned int sensor_value2 = readAverageValue();
    // unsigned int sensor_value_adc_approx = 1.01*sensor_value2*(adc_resolution_val/adc_resolution_vdd);
     unsigned int sensor_value_adc_approx = sensor_value2;
    if (sensor_value_adc_approx > max_battery_level)
    {
        return 100;
    }
    else if (sensor_value_adc_approx < min_battery_level)
    {
        return 0;
    }
    else
    {
        return (((sensor_value_adc_approx - min_battery_level) * percentage_range) / voltage_range) + min_percentage;
    }
}

/*
    Charging and discharging logic:

    1. https://uwspace.uwaterloo.ca/bitstream/handle/10012/12177/Catton_John.pdf
    2. https://core.ac.uk/download/pdf/95355467.pdf
    3. https://core.ac.uk/download/pdf/144148204.pdf
    4. google Lithium-Ion_Polymer_Battery_for_12-Voltage_Applica 
*/

/* 
  Description: this function will read the current flowing to/from the battery charger using the current sensor MCR1101-20-3

  EC:  V_out = Vcc/2 + I_n*66mV/A

  Results (0 current, 12 bits):
    1. Positive: battery is being charge        ---->  2047 0A - 3685   20A
    2. Negative: battery is not being charger   ---->  2047 0A -  409  -20A

  Note: Based on the sign (ADC value) we can infer the status of the charging proccess.

*/


// Module: Battery level reading logic
void batteryTask(void *battery_params)
{
    Serial.println("Battery");
    // Setting up task I/Os
    pinMode(voltage_sensor, INPUT);
    pinMode(power_switch, OUTPUT);
    analogReadResolution(adc_resolution);
    analogSetAttenuation(ADC_11db);

    // Refresh the battery level value for the first time
    float battery_level = readBatteryLevel(); // Battery level variable storage
    // Generate a queue to send battery level parameter
    xBatteryQueue = xQueueCreate(1, sizeof(&battery_level));
    if (xBatteryQueue == nullptr)
    {
       //Serial.println("Error: Battery Queue could not be created");
       //Serial.println("Rebooting...");
        esp_restart();
    }
    
    // Variable to store how much stack is consuming this task
    //UBaseType_t uxHighWaterMark;
    // Task event parameter
    uint32_t ulEventValue;
    // Task event notification receiver
    BaseType_t xEventOccured;

    while (true)
    {
      digitalWrite(power_switch, HIGH);
        if (millis() - battery_check_ref >= battery_interval)
        {
            // Read the battery life and check if the system is allowed to run within the 1-100 percent range. If 0% then it will turn off the system
            battery_level = readBatteryLevel();
            if (battery_level <= min_percentage)
            {
                // Turn off the system when the discharged bound is met
                ESP_LOGI(__func__, "MS-100 Battery Life: %0.2f%% - SHUTTING DOWN SYSTEM", battery_level);
                //digitalWrite(power_switch, LOW);
            }
            else if (battery_level >= safe_boot_percentage)
            {
                digitalWrite(power_switch, HIGH);
                // If the battery level is greater than it's bound (with the error margin), then the battery level remains at 100%
                if (battery_level > max_percentage)
                {
                    battery_level = max_percentage;
                }
            }

            battery_check_ref = millis();
        }

        xEventOccured = xTaskNotifyWait(pdFALSE, battery_task_system_status_bit, &ulEventValue, pdMS_TO_TICKS(1UL));
        // An event occurred?
        if (xEventOccured == pdPASS)
        {
            // Is it a system status event?
            if ((ulEventValue & battery_task_system_status_bit) != 0)
            {
               // Serial.println("Sending battery data to xBatteryQueue...");
                float *xPointerBatteryLevel = &battery_level;
                if (xQueueSendToBack(xBatteryQueue, (void*) &xPointerBatteryLevel, (TickType_t) 0) != pdPASS)
                {
                    Serial.println("Failed to write to xBatteryQueue");
                }
            }
        }

        //uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);  
        //ESP_LOGD(__func__, "uxHighWaterMark:   %u", uxHighWaterMark);
        
        vTaskDelay(battery_task_delay_ms);
    }
}