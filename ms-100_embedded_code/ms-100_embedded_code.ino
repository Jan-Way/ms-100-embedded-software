//===============================================================================================================================================
/*
Project name: MS-100
Project version: 2.0.0 - [insert date] release - [insert commit ID]
Version notes:
File name: ms-100-embedded-software.ino
Description:
*/
//===============================================================================================================================================
#include "battery.h"
#include "flushing.h"
#include "case_temperature.h"
#include "pump.h"
#include "filters.h"
#include "peristaltic.h"
#include "ms_types.h"
#include "logger.h"
#include "WiFiManager.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "rtc_wdt.h"

double FIRWARE_VERSION = 2.0;

#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif

constexpr unsigned long baud_rate = 115200; // Baud rate for the serial monitor
static bool connection_error{true};

SemaphoreHandle_t xMutex = xSemaphoreCreateMutex();
GenericData_t data_to_ota = {FIRWARE_VERSION, xMutex};

// Functions definitions:
void setupSerialDebugger();
void initSystemRoutines();
void setupSerialDebugger()
{
	Serial.begin(baud_rate);
	Serial.println("Hello team! Welcome to the MS-100 firmware debugger!");
	Serial.print("Firmware Version: ");
  Serial.println(FIRWARE_VERSION);
  Serial2.begin(baud_rate);
  ESP_LOGI(__func__, "Hello team! Welcome to the MS-100 firmware debugger!");
  ESP_LOGI(__func__, "Firmware Version: %0.3f", FIRWARE_VERSION);
}

void initSystemRoutines()
{
	// Filters routine task
	xFiltersTaskStatus = xTaskCreatePinnedToCore(
		&filtersTask,		 // Pointer to task
		"filtersTask",		 // Name of the task
		filters_stack_size,  // Task stack size
		NULL,				 // Parameters to the task
		1,					 // Priority
		&xFiltersTaskHandle, // Handler of the task
		ARDUINO_RUNNING_CORE // Core ID
	);

  //WiFiManager routine task
  xWiFiTaskStatus = xTaskCreatePinnedToCore(
    &wifiTask,
    "wifiTask",
    wifi_stack_size,
    NULL,
    1,
    &xWiFiTaskHandle,
    ARDUINO_RUNNING_CORE        
  );

  //Peristaltic routine task
  xPeristalticTaskStatus = xTaskCreatePinnedToCore(
    &peristalticTask, //Pointer to task
    "peristalticTask", // Name of task
    peristaltic_stack_size, // Task stack size
    NULL, // Parameters of the task
    1, // Priority
    &xPeristalticTaskHandle, // Handler of the task
    ARDUINO_RUNNING_CORE // Core ID
  );

	// Battery routine task
	xBatteryTaskStatus = xTaskCreatePinnedToCore(
		&batteryTask,		 // Pointer to task
		"batteryTask",		 // Name of the task
		battery_stack_size,  // Task stack size
		NULL,				 // Parameters to the task
		1,					 // Priority
		&xBatteryTaskHandle, // Handler of the task
		ARDUINO_RUNNING_CORE // Core ID
	);

	// FLushing routine task
	xFlushingTaskStatus = xTaskCreatePinnedToCore(
		&flushingTask,		  // Pointer to task
		"flushingTask",		  // Name of the task
		flushing_stack_size,  // Task stack size
		NULL,				  // Parameters to the task
		1,					  // Priority
		&xFlushingTaskHandle, // Handler of the task
		ARDUINO_RUNNING_CORE  // Core ID
	);

	// Water top off routine task
	xWTOTaskStatus = xTaskCreatePinnedToCore(
		&caseTemperatureTask,			// Pointer to task
		"caseTemperatureTask",			// Name of the task
		wto_stack_size, 			// Task stack size
		NULL,						// Parameters to the task
		1,						  	// Priority
		&xWTOTaskHandle,  	// Handler of the task
		ARDUINO_RUNNING_CORE	  // Core ID
	);

	if ((xFiltersTaskStatus == pdPASS) &&
		(xBatteryTaskStatus == pdPASS) &&
		(xFlushingTaskStatus == pdPASS) &&
		(xWTOTaskStatus == pdPASS) &&
    (xPeristalticTaskStatus == pdPASS) &&
    (xWiFiTaskStatus == pdPASS)
		) 
	{
    //delay(1000);
		Serial.println("All RTOS tasks created");
	}
	else
	{
		Serial.println("An error occurred while creating the RTOS tasks");
	}
}


void initLOGGER(GenericData_t data_param)
{
  // initializing WiFi Manager (and its routines)
  Serial2.begin(baud_rate, SERIAL_8N1, 16, 17);
  DATALOGGER::begin(data_param);

  // Pump routine task
  xPumpTaskStatus = xTaskCreatePinnedToCore(
    &pumpTask,       // Pointer to task
    "pumpTask",      // Name of the task
    pump_stack_size,   // Task stack size
    NULL,        // Parameters to the task
    1,           // Priority
    &xPumpTaskHandle, // Handler of the task
    ARDUINO_RUNNING_CORE // Core ID
  );
}

void setup()
{
  setupSerialDebugger();
	if(xMutex != NULL)
  {
		initSystemRoutines();
    initLOGGER(data_to_ota);
	}
}

void loop() {}
