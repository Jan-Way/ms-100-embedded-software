//===============================================================================================================================================
/*
Project name: MS-100
File name: utilities.cpp
Description: This interface will be used around the SW, providing useful generic tools to do certain algorithms operation
*/
//===============================================================================================================================================

#include "Arduino.h"
#include "utilities.h"
#include <string>
#include <sstream>
#include "string.h"

const char* intToCString(const int &value) 
{
    std::stringstream ss;
    ss << (value);
    return ss.str().c_str(); // Returns a C string with the value already converted
}

void buildKeyName(char *key_name, const size_t &key_name_size, const char* nvs_key, const int &index)
{
    // Clearing key_name buff before use it
    memset(key_name, 0, sizeof(char) * key_name_size);
    // Building the key name...
    strcat(key_name, nvs_key);
    strcat(key_name, "_");
    strcat(key_name, intToCString(index));
}