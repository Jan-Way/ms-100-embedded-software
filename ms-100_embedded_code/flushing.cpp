//===============================================================================================================================================
/*
Project name: MS-100
File name: flushing.cpp
Description: 
*/
//===============================================================================================================================================

#include "Arduino.h"
#include "flushing.h"
#include "logger.h"
#include "ESP32Servo.h"

BaseType_t xFlushingTaskStatus;
TaskHandle_t xFlushingTaskHandle;

static bool flush_flag{true};              // Status flag related to the flushing mechanism counter
bool magnetic_status{true};                // Status flag to know if a magnetic event was triggered
// Variables to perform continuos testing
// Undo comments if testing is carryed out
/*
int global_flush_counter = 0;
int time_bt_flushes = 120000; // 2 minutes
bool stopTime = false;
unsigned long stop_timer_ref = 0;
int time_stop_flushes = 120000; //1200000; // 20 minutes
*/

// Creating a Servo class object
Servo servoMotor;
//Servo servoMotor2;

void setupServoMotor()
{
    // Initializing the Servo motor, attaching the pin associated
    //Serial.println("Initializing the Servo motor, attaching the pin associated");
    servoMotor.attach(servo_motor);
    servoMotor.write(servo_close_value); // Servo Motor default position
}

void readMagneticSensor()
{
    float magnetic_value = 0;
    for (int i = 0; i < 5; ++i) 
    {
      magnetic_value = magnetic_value + digitalRead(magnetic_sensor);
      delay(10);
    }
    magnetic_value = magnetic_value/5;
    if(magnetic_value >= 0.6)
    {
      magnetic_status = true;
    }
    else
    {
      magnetic_status = false;
    }
}
// Module: Flushing mechanism logic
void flushingTask(void *flushing_params)
{
    Serial.println("Flushing");
    // Setting up task I/Os
    pinMode(magnetic_sensor, INPUT);
    setupServoMotor();

    while (true)
    {
        readMagneticSensor();
        if (!magnetic_status && !flush_flag)
        {
            // If they are separated, the motor should move to 180 degrees
            servoMotor.write(180); //servo_open_value
            // delay(5000);
            Serial.println("***FLUSH*** Flushing started...");
            flush_flag = true; // Enabling flush flag
            // Save the actual time as reference
            servo_timer_ref  = millis();
        }

        if (flush_flag)
        {
            if (millis() - servo_timer_ref >= servo_interval)
            {
                // ESP_LOGD(__func__, " After 8 seconds! Flush ended, servo motor is closing down");
                servoMotor.write(0);
                // delay(5000);
                if (magnetic_status)
                {
                    Serial.println("Flushing stopped");
                    flush_flag = false;
                }
            }
        }
        vTaskDelay(flushing_task_delay_ms);
    }
}
