#include <WiFi.h>
#include "WiFiManager.h"

BaseType_t xWiFiTaskStatus;
TaskHandle_t xWiFiTaskHandle;
QueueHandle_t xWiFiQueue;

void wifiTask(void *wifi_params){
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(500);
  }

  while(true){

  }
}