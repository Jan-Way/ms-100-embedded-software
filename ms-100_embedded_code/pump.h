//===============================================================================================================================================
/*
Project name: MS-100
File name: pump.h
Description: 
*/
//===============================================================================================================================================

#ifndef PUMP_H
#define PUMP_H

static constexpr unsigned long pump_stack_size{2048}; //2048
static constexpr TickType_t pump_task_delay_ms{pdMS_TO_TICKS(1UL)};
static constexpr unsigned int dirty_water_pump_status_bit{0x20};

static constexpr int pump_float_sensor{GPIO_NUM_5}; // Float sensor associated to the pump
static constexpr int pump{GPIO_NUM_13};              // System's pump
//static constexpr int fan{GPIO_NUM_14};  // FAN Case Control
// static constexpr int current_sensor{GPIO_NUM_21}; 

static constexpr int dirty_float_sensor{GPIO_NUM_2};    // System's float sensor associated to the dirty basin. Digital input

static constexpr unsigned int max_current_sensor{6}; // Max current value that should flow through the pump

constexpr unsigned long turn_on_pump_interval{2500UL};  // 5.5 seconds
constexpr unsigned long turn_off_pump_interval{1000UL}; // 4 seconds
constexpr unsigned long empty_reservoir_interval{2500UL}; // 3 minutes
// static constexpr TickType_t queue_wait_time{100}; // 100ms

extern BaseType_t xPumpTaskStatus;
extern TaskHandle_t xPumpTaskHandle;
extern QueueHandle_t xdirtyWaterQueue;

void pumpTask(void *pump_params);

#endif