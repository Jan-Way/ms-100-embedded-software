//===============================================================================================================================================
/*
Project name: MS-100
File name: filters.cpp
Description: 
*/
//===============================================================================================================================================

#include "Arduino.h"
#include "filters.h"
#include "ms_types.h"
#include "esp32-hal-adc.h"  // needed for adc pin reset
#include "soc/sens_reg.h"   // needed for adc pin reset, needed for manipulating ADC2 control register
#include <iostream>
#include <unordered_map>


BaseType_t xFiltersTaskStatus;
TaskHandle_t xFiltersTaskHandle;
QueueHandle_t xPressureValuesQueue;
QueueHandle_t xFilterStatusQueue;

static unsigned long filters_check_ref{0};  // Tiner reference for the battery routine counter
static bool filters_check_flag{false};      // Status flag related to the battery check routine
bool pressure_status_check_flag{false};      // Status flag related to the battery check routine

/**
 * @name: readPressureSensor
 * @description: read pressure sensor value from a selected analog pin and returns the value in PSI units
 * @parameters:
 *      - sensor_pin: analog pin number to read from,
 *      - sensor_number: number of the sensor (debug purposes only)
 * @return: pressure in PSI as float
*/
const float readPressureSensor(const int &sensor_pin, const int &sensor_number)
{
    /* OUTPUT: 
    output is linear 0,5-4,5 V. 
    0  psi - 0,5 V
    30 psi - 2,5 V 
    60 psi - 4,5 V
    */
    ESP_LOGD(__func__, "GPIO: %i", sensor_pin);
    int sensor_value = analogRead(sensor_pin);
    float sensor_value_voltage_approx = sensor_value;
    // float sensor_value_voltage_approx = -0.000000000000016 * pow(sensor_value,4) + 0.000000000118171 * pow(sensor_value,3)- 0.000000301211691 * pow(sensor_value,2)+ 0.001109019271794 * sensor_value + 0.034143524634089;
    ESP_LOGD(__func__, "pressure_sensor_%i ADC: %i", sensor_number, sensor_value_voltage_approx);
    int const_factor = 1.388; // Define during testing
    float voltage = (const_factor*sensor_value_voltage_approx * adc_resolution_vdd) / adc_resolution_val;
    ESP_LOGD(__func__, "pressure_sensor_%i voltage: %0.3f", sensor_number, voltage);

    // When Pressure = 0, Analog Input = TO BE DEFINE IN TESTING  ---- 412
    // Conversion of Analog Input to Voltage: Analog Input = 412 -> Voltage = 412*(5/2047) = 0.5
    // constexpr float m = ((60 - 0) / (4.5 - 0.5));
    // constexpr float b = 60 - (m * 4.5);
    // During testing of version 2.1, this linear equation didnt work
    // So, extracting data it was found the slope and the bias manually
    // for version 3.2 we will need to do it again
    constexpr float m = 31.088;
    constexpr float b = -7.02;
    ESP_LOGV(__func__, "m = %.3f", m);
    ESP_LOGV(__func__, "b = %.3f", b);

    float pressure_psi = ((m * 1.3*voltage) + b);
    if (pressure_psi < 0) {
        pressure_psi = 0;
    }
    ESP_LOGI(__func__, "pressure_sensor_%i pressure: %.3fpsi", sensor_number, pressure_psi);

    return pressure_psi;
}

void getPressureValues(std::unordered_map<int, float> &pressure_values)
{
    for (auto &element : pressure_sensors_pins)
    {
        pressure_values[element.first] = readPressureSensor(element.second, element.first);
    }
}

// Module: Flushing mechanism logic
void filtersTask(void *filters_params)
{
    Serial.println("Filters");
    // Setting up task I/Os
    // Iterate over the map using c++11 range based for loop
    for (auto element : pressure_sensors_pins)
    {
        pinMode(element.second, INPUT);
        adcAttachPin(element.second);
        ESP_LOGV(__func__, "Pin set: %i", element.second);
    }
    analogReadResolution(adc_resolution);
    // analogSetAttenuation(ADC_11db);

    xFilterStatusQueue = xQueueCreate(1, sizeof(&pressure_status_check_flag));
    if (xFilterStatusQueue == nullptr)
    {
      //  Serial.println("Error: Preassure status Queue could not be created");
       // Serial.println("Rebooting...");
        esp_restart();
    }
    // Unordered_map to store each pressure sensors values
    std::unordered_map<int, float> pressure_values;
    // Generate a queue to send pressure values parameters
    xPressureValuesQueue = xQueueCreate(1, sizeof(&pressure_values));
    if (xPressureValuesQueue == nullptr)
    {
       // Serial.println("Error: Pressure Values Queue could not be created");
       // Serial.println("Rebooting...");
        esp_restart();
    }
    // Refresh the pressure sensors values for the first time
    getPressureValues(pressure_values);
    // Variable to store how much stack is consuming this task
    //UBaseType_t uxHighWaterMark;

    while (true)
    {
        if (!filters_check_flag)
        {
            filters_check_flag = true; // Enabling filters check flag
            // Save the actual time as reference
            filters_check_ref = millis();
        }

        if (filters_check_flag)
        {
            if (millis() - filters_check_ref >= filters_check_interval)
            {
                getPressureValues(pressure_values);
                filters_check_flag = false;

                // The following logic will determine if any of the filters has a value upper to the limit
                float max_pressure_value = 0;
                for (auto x : pressure_values)
                {
                    if (max_pressure_value < x.second) 
                    {
                        max_pressure_value = x.second;
                    }
                }
                ESP_LOGD(__func__, "**FILTER TASK** max_pressure_value: %f", max_pressure_value);
                if (max_pressure_value >= max_security_pressure) 
                {
                    pressure_status_check_flag = true;
                }
                else
                {
                    pressure_status_check_flag = false;
                }
            }
        }

        uint32_t ulEventValue2;
        BaseType_t xEventOccured2 = xTaskNotifyWait(pdFALSE, filters_task_pump_status_bit, &ulEventValue2, pdMS_TO_TICKS(1UL));
        // An event occurred?
        if (xEventOccured2 == pdPASS)
        {
            // Is it a system status event?
            if ((ulEventValue2 & filters_task_pump_status_bit) != 0)
            {
                //ESP_LOGD(__func__, "Sending max preassure status data to xFilterStatusQueue...");
                bool *xPointerPreassureStatus = &pressure_status_check_flag;
                if (xQueueSendToBack(xFilterStatusQueue, (void*) &xPointerPreassureStatus, (TickType_t) 0) != pdPASS)
                {
                  //  Serial.println("Failed to write to xFilterStatusQueue");
                }
            }
        }    

        // Checking if a new flush event occurred
        uint32_t ulEventValue;
        BaseType_t xEventOccured = xTaskNotifyWait(pdFALSE, filters_task_flush_bit, &ulEventValue, pdMS_TO_TICKS(1UL));
        // An event occurred?
        if (xEventOccured == pdPASS)
        {
            // Is it a Flush event?
            if ((ulEventValue & filters_task_flush_bit) != 0)
            {
                //ESP_LOGD(__func__, "Sending pressure sensors data to xPressureValuesQueue...");
                std::unordered_map<int, float> *xPointerPressureValues = &pressure_values;
                if (xQueueSendToBack(xPressureValuesQueue, (void*) &xPointerPressureValues, (TickType_t) 0) != pdPASS)
                {
                  //  Serial.println("Failed to write to xPressureValuesQueue");
                }
            }
        }
        vTaskDelay(filters_task_delay_ms);
    }
}