//===============================================================================================================================================
/*
Project name: MS-100
File name: case_temperature.cpp
Description: 
*/
//===============================================================================================================================================

#include "Arduino.h"
#include "case_temperature.h"
#include "ms_types.h"
#include "OneWire.h"    
#include "DallasTemperature.h"
#include <esp_adc_cal.h>             

//#include "soc/timer_group_struct.h"
//#include "soc/timer_group_reg.h"

BaseType_t xWTOTaskStatus;
TaskHandle_t xWTOTaskHandle;
QueueHandle_t xCaseTemperatureQueue;

static bool fan_check_flag{false};   // Flag to check if the fan is active or not TRUE=active
static float max_security_temp{40.0};   // Max PCB temperature value in Celsius     
static float min_security_temp{30.0};    // Min PCB temperature value in Celsius
// static bool restart_flag{false};
// static unsigned long restart_timer_ref{0};

//Reads the raw data of the temperature sensor and cleans it for a more stable reading.
uint32_t readADC(int ADC_raw)
{
  esp_adc_cal_characteristics_t adc_chars;
  esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc_chars);
  return(esp_adc_cal_raw_to_voltage(ADC_raw, &adc_chars)); 
}

float readCaseTemperature()
{
    float raw_sensor_value = analogRead(temp_sensor);
    float voltage = readADC(raw_sensor_value);
    float temp_value = voltage / 10;
    Serial.println(temp_value);
    return temp_value;
}

// Module: Water top off mechanism logic
void caseTemperatureTask(void *wto_params)
{
    Serial.println("Temperature");
    // Setting up task I/Os
    pinMode(temp_sensor, INPUT);
    // pinMode(reset_button, INPUT);
    pinMode(fan_pcb, OUTPUT);
   
    // Refresh the solenoid temperature value for the first time (in ºC)
    float case_temperature = readCaseTemperature(); // Stores the actual temperature value of the solenoid
    // Generate a queue to send solenoid temperature parameter
    xCaseTemperatureQueue = xQueueCreate(1, sizeof(&case_temperature));
    if (xCaseTemperatureQueue == nullptr)
    {
       // Serial.println("Error: Case Temperature Queue could not be created");
        //Serial.println("Rebooting...");
        esp_restart();
    }

    // Variable to store how much stack is consuming this task
    // UBaseType_t uxHighWaterMark;
    // Task event parameter
    uint32_t ulEventValue;
    // Task event notification receiver
    BaseType_t xEventOccured;

    while (true)
    {
        case_temperature = readCaseTemperature();
        if(fan_check_flag){

            if(case_temperature < min_security_temp){
                ESP_LOGD(__func__, "Case temperature is back to normal, turning off the fan: %0.2f", case_temperature);
                digitalWrite(fan_pcb, LOW);
                fan_check_flag = false; 
            }
        }
        else{
            if (case_temperature >= max_security_temp){
                ESP_LOGD(__func__, "Case temperature is too high, turning on the fan: %0.2f", case_temperature);
                digitalWrite(fan_pcb, HIGH);
                fan_check_flag = true;
            }
            // else{
            //     ESP_LOGD(__func__, "NOT GETTING IN WHY");
                
            // }
        }

        xEventOccured = xTaskNotifyWait(pdFALSE, wto_task_system_status_bit, &ulEventValue, pdMS_TO_TICKS(1UL));
        // An event occurred?
        if (xEventOccured == pdPASS)
        {
            // Is it a system status event?
            if ((ulEventValue & wto_task_system_status_bit) != 0)
            {
               // Serial.println("Sending case temperature data to xCaseTemperatureQueue...");
                float *xPointerCaseTemperature = &case_temperature;
                if (xQueueSendToBack(xCaseTemperatureQueue, (void*) &xPointerCaseTemperature, (TickType_t) 0) != pdPASS)
                {
                 //   Serial.println("Failed to write to xCaseTemperatureQueue");
                }
            }
        }
        vTaskDelay(wto_task_delay_ms);
    }
}
