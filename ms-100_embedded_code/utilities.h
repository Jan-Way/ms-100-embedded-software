//===============================================================================================================================================
/*
Project name: MS-100
File name: utilities.h
Description: 
*/
//===============================================================================================================================================

#ifndef UTILITIES_H
#define UTILITIES_H

const char* intToCString(const int &value);
void buildKeyName(char* key_name, const size_t &key_name_size, const char* nvs_key, const int &index);

#endif