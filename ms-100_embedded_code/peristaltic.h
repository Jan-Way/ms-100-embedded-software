//===============================================================================================================================================
/*
Project name: MS-100
File name: peristaltic.h
Description: 
Everytime the magnetic switch is actionated, the peristaltic pump will begin a countdown, in this case is 10 min, when the countdown finishes, the peristaltic pump activates.
*/
//===============================================================================================================================================

#ifndef PERISTALTIC_H
#define PERISTALTIC_H

#define peristaltic_pump GPIO_NUM_14 // Pin number 14 on ESP32
#define push_button GPIO_NUM_4 // Pin number 4 on ESP32

static constexpr unsigned long peristaltic_stack_size{1400};
static constexpr TickType_t peristaltic_task_delay_ms{pdMS_TO_TICKS(1UL)};
static constexpr unsigned int peristaltic_task_system_status_bit{0x15};

static bool magnetic_flag{true}; 
static bool magnetic_switch_status{false};
static bool mag_status{true};

static bool peristaltic_flag{true};
static unsigned long peristaltic_on_ref{0UL}; //Reference to the time interval
static constexpr unsigned long peristaltic_on_interval{480000UL}; //8 min / 480000ms / Time interval between every run 

static bool peristaltic_time_flag{true}; 
static unsigned long peristaltic_off_ref{0UL}; //reference to the general dose
static constexpr unsigned long peristaltic_off_interval{1250UL}; //Time of dose

static bool status{true};
static unsigned long reference{0UL};
static unsigned long interval{10000UL};

static unsigned long button_ref{0UL};
static constexpr unsigned long button_interval{2500UL}; //Time of dose when the button is pressed

static bool push_button_status{false};
static bool push_button_pressed{false};
static bool banned{false};
static bool flag{false};

static unsigned long push_ref{0};
static unsigned long push_interval{250};

static unsigned long flag_ref{0};
static bool reference_flag{true};

extern BaseType_t xPeristalticTaskStatus;
extern TaskHandle_t xPeristalticTaskHandle;
extern QueueHandle_t xPeristalticQueue;

void peristalticTask(void *peristaltic_params);

#endif