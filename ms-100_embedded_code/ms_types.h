//===============================================================================================================================================
/*
Project name: MS-100
File name: ms_types.h
Description: 
*/
//===============================================================================================================================================

#ifndef MS_TYPES_H
#define MS_TYPES_H

#include "freertos/semphr.h"
//================================================================================
// WIFI TYPES:

/* MS WIFI debug type of status */
enum class MSWiFiDebug
{
    MS_WIFI_IDLE,
    MS_WIFI_CONNECTED,
    MS_WIFI_DISCONNECTED,
    MS_WIFI_FAIL,    // This status is used for the gateway and WPS
    MS_WIFI_SUCCESS, // This status is used for the gateway and WPS
    MS_WIFI_ERROR
};

/* MS WiFi Network types*/
enum class MSWiFiAuthType
{
    MS_WIFI_AUTH_PSK,
    MS_WIFI_AUTH_ENTERPRISE,

    // IDLE
    NONE
};
//================================================================================
//================================================================================
// API TYPES:

/* Registration debug type of status */
enum class RegistrationDebug {
	MS_REGISTRATION_SUCCESS,
	MS_REGISTRATION_FAILED,
    MS_UNREGISTERED,
    MS_REGISTERED
};

/* Event Report types */
enum class EventReportType {
    SYSTEM_STATUS,
    FLUSH,
    CREDENTIALS_DIGESTED,
    CREDENTIALS_REJECTED,
    
    NONE
};

enum class ClockDebug {
	CLOCK_INIT_SUCCESS,
	CLOCK_INIT_FAILED
};
//================================================================================
//================================================================================
// NVS TYPES:

/* MS WIFI debug type of status */
enum class NVSDebug
{
    // WiFi list status in NVS
    NVS_WIFI_LIST_EXIST,
    NVS_WIFI_LIST_FULL,
    NVS_WIFI_LIST_EMPTY,
    // Device ID status in NVS
    NVS_DEVICE_ID_EXIST,
    NVS_DEVICE_ID_EMPTY,
    // Json Web Token (JWT) status in NVS
    NVS_JWT_EXIST,
    NVS_JWT_EMPTY,
    // Events logs status in NVS
    NVS_EVENT_LOGS_EXIST,
    NVS_EVENT_LOGS_FULL,
    NVS_EVENT_LOGS_EMPTY,

    // IDLE
    NONE
};
//================================================================================
//================================================================================
// ANALOG types:

//static constexpr unsigned int adc_resolution{11};
//static constexpr unsigned int adc_resolution_val{2047};
static constexpr unsigned int adc_resolution{12};
static constexpr unsigned int adc_resolution_val{4095};
static constexpr unsigned int adc_resolution_vdd{3.3};

//================================================================================
//================================================================================
// Parameter to tasks:
typedef struct Data_t
{
    double firmwareVersion;
    SemaphoreHandle_t semaphor;
} GenericData_t;

//================================================================================
//================================================================================
/* MS WIFI debug type of status */
enum class MSWiFiOTADebug
{
    MS_OTA_IDLE,
    MS_WIFI_START,
    MS_WIFI_LOOKING,
    MS_WIFI_CONNECTED,
    MS_WIFI_DISCONNECTED,
    MS_WIFI_DEINIT,   
    MS_OTA_SUCCESS, 
    MS_OTA_ERROR,
    MS_OTA_NO_RESTART,
    MS_OTA_DOWNLOADING,
};
#endif