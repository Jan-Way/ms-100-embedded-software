//===============================================================================================================================================
/*
Project name: MS-100
Project version: 2.0.0 - [insert date] release - [insert commit ID]
Version notes:
File name: ms-100-embedded-software.ino
Description:
*/
//===============================================================================================================================================

#include "src/includes/battery.h"
#include "src/includes/flushing.h"
#include "src/includes/case_temperature.h"
#include "src/includes/pump.h"
#include "src/includes/filters.h"
#include "src/includes/ms_types.h"
#include "src/includes/logger.h"
#include "src/includes/arduino_esp/includes/include/nvs_flash/nvs_flash.h"
#include "src/includes/arduino_esp/includes/freertos/FreeRTOS.h"
#include "src/includes/arduino_esp/includes/freertos/semphr.h"
#include "src/includes/arduino_esp/includes/include/soc/soc/rtc_wdt.h"

double FIRWARE_VERSION = 1.1;

#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif

constexpr unsigned long baud_rate = 115200; // Baud rate for the serial monitor
static bool connection_error{true};

SemaphoreHandle_t xMutex = xSemaphoreCreateMutex();
GenericData_t data_to_ota = {FIRWARE_VERSION, xMutex};

// Functions definitions:
void setupSerialDebugger();
void initWiFi();
void initSystemRoutines();
void setupSerialDebugger()
{
	Serial.begin(baud_rate);
	ESP_LOGI(__func__, "Hello team! Welcome to the MS-100 firmware debugger!");
	ESP_LOGI(__func__, "Firmware Version: %0.3f", FIRWARE_VERSION);
   Serial2.begin(baud_rate);
   ESP_LOGI(__func__, "Hello team! Welcome to the MS-100 firmware debugger!");
   ESP_LOGI(__func__, "Firmware Version: %0.3f", FIRWARE_VERSION);
}

void initNVS()
{
	const esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES)
	{
		ESP_LOGE(__func__, "A problem occured initializing NVS...");
		/* NVS partition was truncated
		* and needs to be erased */
		ESP_ERROR_CHECK(nvs_flash_erase());

		/* Retry nvs_flash_init */
		ESP_ERROR_CHECK(nvs_flash_init());
	}
}

void initSystemRoutines()
{
	// Filters routine task
	xFiltersTaskStatus = xTaskCreatePinnedToCore(
		&filtersTask,		 // Pointer to task
		"filtersTask",		 // Name of the task
		filters_stack_size,  // Task stack size
		NULL,				 // Parameters to the task
		1,					 // Priority
		&xFiltersTaskHandle, // Handler of the task
		ARDUINO_RUNNING_CORE // Core ID
	);

	// Battery routine task
	xBatteryTaskStatus = xTaskCreatePinnedToCore(
		&batteryTask,		 // Pointer to task
		"batteryTask",		 // Name of the task
		battery_stack_size,  // Task stack size
		NULL,				 // Parameters to the task
		1,					 // Priority
		&xBatteryTaskHandle, // Handler of the task
		ARDUINO_RUNNING_CORE // Core ID
	);

	// FLushing routine task
	xFlushingTaskStatus = xTaskCreatePinnedToCore(
		&flushingTask,		  // Pointer to task
		"flushingTask",		  // Name of the task
		flushing_stack_size,  // Task stack size
		NULL,				  // Parameters to the task
		1,					  // Priority
		&xFlushingTaskHandle, // Handler of the task
		ARDUINO_RUNNING_CORE  // Core ID
	);

	// Water top off routine task
	xWTOTaskStatus = xTaskCreatePinnedToCore(
		&caseTemperatureTask,			// Pointer to task
		"caseTemperatureTask",			// Name of the task
		wto_stack_size, 			// Task stack size
		NULL,						// Parameters to the task
		1,						  	// Priority
		&xWTOTaskHandle,  	// Handler of the task
		ARDUINO_RUNNING_CORE	  // Core ID
	);

//	// Pump routine task
//	xPumpTaskStatus = xTaskCreatePinnedToCore(
//		&pumpTask,			 // Pointer to task
//		"pumpTask",			 // Name of the task
//		pump_stack_size,	 // Task stack size
//		NULL,				 // Parameters to the task
//		1,					 // Priority
//		&xPumpTaskHandle,	// Handler of the task
//		ARDUINO_RUNNING_CORE // Core ID
//	);

	if ((xFiltersTaskStatus == pdPASS) &&
		(xBatteryTaskStatus == pdPASS) &&
		(xFlushingTaskStatus == pdPASS) &&
		(xWTOTaskStatus == pdPASS) 
//		(xPumpTaskStatus == pdPASS) &&
		) 
	{
		ESP_LOGD(__func__, "All RTOS tasks created");
	}
	else
	{
		ESP_LOGE(__func__, "An error occurred while creating the RTOS tasks");
	}
}


void initLOGGER(GenericData_t data_param)
{
  // initializing WiFi Manager (and its routines)
  Serial2.begin(baud_rate, SERIAL_8N1, 16, 17);
  DATALOGGER::begin(data_param);

  // Pump routine task
  xPumpTaskStatus = xTaskCreatePinnedToCore(
    &pumpTask,       // Pointer to task
    "pumpTask",      // Name of the task
    pump_stack_size,   // Task stack size
    NULL,        // Parameters to the task
    1,           // Priority
    &xPumpTaskHandle, // Handler of the task
    ARDUINO_RUNNING_CORE // Core ID
  );
}
/*
void initOTA(GenericData_t data_param)
{
  // ESP_LOGD(__func__, "current firmware*** (%.1f)SET UP...\n", data_param.firmwareVersion);
  // Fan routine task
  xOtaTaskStatus = xTaskCreatePinnedToCore(
    &otaTask,      // Pointer to task
    "otaTask",       // Name of the task
    ota_stack_size,  // Task stack size
    (void*)&data_param,        // Parameters to the task
    3,           // Priority
    &xOtaTaskHandle,  // Handler of the task
    ARDUINO_RUNNING_CORE // Core ID
  );
} */  

void setup()
{
  setupSerialDebugger();
	if(xMutex != NULL)
	{
		initNVS();
		initSystemRoutines();
    initLOGGER(data_to_ota);
    //delay(100);
    //initWiFi();
    //initOTA(data_to_ota);
	}
}

void loop() {}
