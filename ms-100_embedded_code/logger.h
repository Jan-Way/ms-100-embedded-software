//===============================================================================================================================================
/*
Project name: MS-100
File name: logger.h
Description: 
*/
//===============================================================================================================================================

#ifndef LOGGER_H
#define LOGGER_H

#include "freertos/FreeRTOS.h"
#include "ms_types.h"
#include <unordered_map>
#include <WiFi.h>
#include "driver/adc.h"
#include "esp_wifi.h"

static constexpr unsigned long logger_stack_size{30240};
static constexpr unsigned int logger_task_system_status_bit{0x04};
static constexpr unsigned int logger_task_flush_bit{0x01};
static constexpr TickType_t logger_task_delay_ms{pdMS_TO_TICKS(100UL)}; // 100ms
static constexpr TickType_t Queue_current_wait_time{250}; // 100ms

static constexpr unsigned long System_status_interval{6000UL};//60000UL};

static constexpr int current_sensor_pin{GPIO_NUM_15};  // System's power switch digital output

static float maxRawData{0};
static unsigned int raw_data{0};

extern BaseType_t xLOGGERTaskStatus;
extern TaskHandle_t xLOGGERTaskHandle;
extern QueueHandle_t xCurrentQueue;

//Peristaltic send_data variables and definitions ================================================================================================================================

static const char* ntpServer = "pool.ntp.org";
static const long  gmtOffset_sec = 21600;
static const int   daylightOffset_sec = 0;

// Google script ID and required credentials
static String GOOGLE_SCRIPT_ID = "AKfycbx9iBvjUZZAeRTwOGlTTosc1zCKMJHstEEM0uhzE_wX_EWBYM-ZKiNTNzkgA7kWq7XW";    // change Gscript ID with yours

//Ends send_data pump code ================================================================================================================================

void loggerTask(void *logger_params);

class DATALOGGER
{
public:
    // Public methods
    static void begin(GenericData_t data_param);
};

#endif