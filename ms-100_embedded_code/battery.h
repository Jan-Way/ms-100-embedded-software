//===============================================================================================================================================
/*
Project name: MS-100
File name: battery.h
Description: 
*/
//===============================================================================================================================================

#ifndef BATTERY_H
#define BATTERY_H

static constexpr unsigned long battery_stack_size{2048};
static constexpr unsigned int battery_task_system_status_bit{0x02};
static constexpr unsigned int battery_charge_task_system_status_bit{0x08};

static constexpr TickType_t battery_task_delay_ms{pdMS_TO_TICKS(1UL)}; // 1ms
static constexpr unsigned long battery_interval{6000UL}; // 10s

static constexpr unsigned int min_battery_level{3201};  // Minimun voltage of the battery represented as ADC value (11-bits resolution)
static constexpr unsigned int max_battery_level{3375};  // Maximun voltage of the battery represented as ADC value (11-bits resolution)
static constexpr unsigned int min_percentage{0};        // Minimun value percentage for battery level
static constexpr unsigned int max_percentage{100};      // Maximun value percentage for battery level
static constexpr unsigned int safe_boot_percentage{1}; // Safe battery level percentage value to boot the system

static constexpr unsigned int voltage_range{max_battery_level - min_battery_level};
static constexpr unsigned int percentage_range{max_percentage - min_percentage};

static constexpr int voltage_sensor{GPIO_NUM_33}; // Voltage sensor pin to read the battery voltage. Analog input
static constexpr int power_switch{GPIO_NUM_25};  // System's power switch digital output 

extern BaseType_t xBatteryTaskStatus;
extern TaskHandle_t xBatteryTaskHandle;
extern QueueHandle_t xBatteryQueue;

float readBatteryLevel();
void readChargingState();
void batteryTask(void *battery_params);

#endif