//===============================================================================================================================================
/*
Project name: MS-100
File name: filters.h
Description: 
*/
//===============================================================================================================================================

#ifndef FILTERS_H
#define FILTERS_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include <map>

static constexpr unsigned long filters_stack_size{2304};
static constexpr unsigned int filters_task_flush_bit{0x01};
static constexpr unsigned int filters_task_pump_status_bit{0x10};
static constexpr TickType_t filters_task_delay_ms{pdMS_TO_TICKS(1UL)};

static constexpr unsigned long filters_check_interval{6000UL}; // 10 seconds
const unsigned int max_security_pressure = 70;   //Max system pressure value in PSI

// Initialize a Map of string & int using initializer_list
static const std::map<int, int> pressure_sensors_pins {
    {1, GPIO_NUM_15}, // System's pressure sensor 1 digital input
    {2, GPIO_NUM_27}, // System's pressure sensor 2 digital input
    {3, GPIO_NUM_34}, // System's pressure sensor 3 digital input
    {4, GPIO_NUM_35}  // System's pressure sensor 4 digital input
};

extern BaseType_t xFiltersTaskStatus;
extern TaskHandle_t xFiltersTaskHandle;
extern QueueHandle_t xPressureValuesQueue;
extern QueueHandle_t xFilterStatusQueue;

void filtersTask(void *filters_params);

#endif