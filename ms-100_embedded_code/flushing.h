//===============================================================================================================================================
/*
Project name: MS-100
File name: flushing.h
Description: 
*/
//===============================================================================================================================================

#ifndef FLUSHING_H
#define FLUSHING_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

static constexpr unsigned long flushing_stack_size{1792};
static constexpr TickType_t flushing_task_delay_ms{pdMS_TO_TICKS(1UL)};

static constexpr unsigned long servo_interval{8500UL}; // 8.5 seconds
static unsigned long servo_timer_ref{0};    // Timer reference for the servo motor counter
// static constexpr unsigned long servo_interval{1000UL};
static constexpr unsigned int servo_close_value{0U};   // Default value to close the flushing lid
static constexpr unsigned int servo_open_value{180U};  // Default value to open the flushing lid

static constexpr int magnetic_sensor{GPIO_NUM_18}; // System's magnetic sensor digital input
static constexpr int servo_motor{GPIO_NUM_26};     // System's servo motor digital output
//static constexpr int servo_motor2{GPIO_NUM_4};     // System's servo motor digital output

extern BaseType_t xFlushingTaskStatus;
extern TaskHandle_t xFlushingTaskHandle;

void readMagneticSensor();
void flushingTask(void *flushing_params);

#endif