//===============================================================================================================================================
//  Project name: MS-100
//  File name: peristaltic.cpp
//
//  Description: 
//===============================================================================================================================================

#include "Arduino.h"
#include "flushing.h"
#include "peristaltic.h"

BaseType_t xPeristalticTaskStatus;
TaskHandle_t xPeristalticTaskHandle;
QueueHandle_t xPeristalticQueue;

//Reads the state of the magnetic sensor
 void readSensor(){
    if(!digitalRead(magnetic_sensor)){
      magnetic_switch_status = true;
      mag_status = true;
    } else {
      mag_status = false;
    }
}

//Function to read the button
void readPushButton(){
  if(digitalRead(push_button)){
    push_button_pressed = true;
    button_ref = millis();
    push_ref = millis();
  }
}

//Funciton that initializes the task for the peristaltic pump
void peristalticTask(void *peristaltic_params){
  Serial.println("Peristaltic");

  pinMode(peristaltic_pump, OUTPUT);
  pinMode(push_button, INPUT);
  
  // Variable to store how much stack is consuming this task
 // UBaseType_t uxHighWaterMark;
  // Task event parameter
  uint32_t ulEventValue;
  // Task event notification receiver
  BaseType_t xEventOccured;

  while(true){
    // Read the sensor push button
    readPushButton();

    // Read the state of the magnetic switch and store the bool value into a variable
    if(magnetic_switch_status){
      if(millis() - flag_ref >= 1000){
        flag = true;
        digitalWrite(peristaltic_pump, LOW);
      }   
      if(!mag_status){
        magnetic_switch_status = true;
        flag = false;
      }
    }
    // If the banned variable is true, it will stop reading the magnetic sensor.
    if(!banned){
      readSensor();
    }    
    // This condition helps the activation of the pump only when the magnetic switch goes from the on to off position.
    if(magnetic_switch_status && !flag){
      //Take reference of the on time, entering just once to stop taking the reference of the time many times.
        if(peristaltic_flag){
          peristaltic_on_ref = millis();
          peristaltic_flag = false;
        }
        if(peristaltic_time_flag){
            peristaltic_off_ref = millis();
            peristaltic_time_flag = false; 
          }
          digitalWrite(peristaltic_pump, HIGH);
          banned = true;
          // How much time the pump will run.
          if(millis() - peristaltic_off_ref >= peristaltic_off_interval){        
            digitalWrite(peristaltic_pump, LOW);
            magnetic_switch_status = false;
            peristaltic_flag = true;
            peristaltic_time_flag = true;
          }
      // The pump will be running only when you push the botton and fully realese it.
      if(push_button_pressed){
        if(millis() - push_ref >= push_interval && !digitalRead(push_button)){
          digitalWrite(peristaltic_pump, HIGH); 
        }
        if(millis() - button_ref >= button_interval){
          push_button_status = true;
          push_button_pressed = false;
        }
      } else if(push_button_status){
        digitalWrite(peristaltic_pump, LOW);
        push_button_status = false;
      }
    } else {
      flag_ref = millis();
      if(push_button_pressed){
        if(millis() - push_ref >= push_interval && !digitalRead(push_button)){
          digitalWrite(peristaltic_pump, HIGH); 
        }
        if(millis() - button_ref >= button_interval){
          push_button_status = true;
          push_button_pressed = false;
        }            
        } else if(push_button_status){
          digitalWrite(peristaltic_pump, LOW);
          push_button_status = false;
      }
    }
    // Every time that the magnetic sensor stays open, the reading funtion would not be activated.
    if(millis() - peristaltic_on_ref >= peristaltic_on_interval){
      banned = false;      
    }
    xEventOccured = xTaskNotifyWait(pdFALSE, peristaltic_task_system_status_bit, &ulEventValue, pdMS_TO_TICKS(1UL));
    // An event occurred?
    if (xEventOccured == pdPASS)
    {
      // Is it a system status event?
      if ((ulEventValue & peristaltic_task_system_status_bit) != 0)
      {
        // Serial.println("Sending case temperature data to xPeristalticQueue...");
        bool *xPointerPeristalticState = &magnetic_switch_status;
        if (xQueueSendToBack(xPeristalticQueue, (void*) &xPointerPeristalticState, (TickType_t) 0) != pdPASS)
        {
          //   Serial.println("Failed to write to xPeristalticQueue");
        }
      }
    }
    //Know the stack size of this task
    //uxHighWaterMark = uxTaskGetStackHighWaterMark(NULL);  
    //Serial.print("uxHighWaterMark: ");
    //Serial.println(uxHighWaterMark);
    vTaskDelay(peristaltic_task_delay_ms);
  }  
}