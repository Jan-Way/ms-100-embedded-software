//===============================================================================================================================================
//  Project name: MS-100
//  File name: DATALOGGER.cpp
//
//  Description: Data we need to collect and send to the RPi using UART
//
//      * Batery power
//      * Case temperature
//      * Timestamp
//      * Filter status
//
//      * Update signal
//
//
//===============================================================================================================================================

// #include "includes/arduino_esp/esp32/ESP32.h"
#include "logger.h"
#include "filters.h"
#include "battery.h"
//#include "Clock.h"
#include "pump.h"
#include "case_temperature.h"
#include "flushing.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "cJSON.h"
#include "ArduinoJson.h"
//#include "includes/arduino_esp/includes/include/esp_http_client/esp_http_client.h" 
//#include "esp_http_client.h"
//#include "includes/arduino_esp/includes/include/esp_https_ota/esp_https_ota.h"
//#include "esp_https_ota.h"
//#include "nvs_flash.h"
//#include "rtc_wdt.h"
//#include "esp_task_wdt.h"

#include <string>
#include <sstream>
#include <iomanip>
#include <unordered_map>
#include <vector>
#include "Wire.h"
#include "esp32-hal-adc.h"  // needed for adc pin reset
#include "soc/sens_reg.h"

#include <HTTPClient.h>
#include "time.h"

//#include <ArduinoJson.h>

#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif

BaseType_t xLOGGERTaskStatus;
TaskHandle_t xLOGGERTaskHandle;
QueueHandle_t xCurrentQueue;

extern float readCaseTemperature();

/*double global_firmware_version = 0.0;
StaticJsonDocument<1024> serial_payload;
bool available_update_status = false;
bool restart_status = false;
bool charging_state = false;
MSWiFiOTADebug update_status = MSWiFiOTADebug::MS_OTA_IDLE;
int times_to_reconnect_ota = 0;*/
/*

    WIRING DIAGRAM
    --------------
                                        MCP3221
                                        -------
                                    VCC --| •     |-- SCL
                                        |       |
                                    GND --|       |
                                        |       |
                                    AIN --|       |-- SDA
                                        -------
    PIN 1 (VCC/VREF) - Serves as both Power Supply input and Voltage Reference for the ADC. Connect to ESP32 3.3V output or any other
                    equivalent power source (5.5V max). If using an external power source, remember to connect all GND's together
    PIN 2 (GND) - connect to ESP32 GND
    PIN 3 (AIN) - Connect to ESP32's 3.3V Output or to the middle pin of a 10K potentiometer (the pot's first pin goes to GND and the third to 5V)
    PIN 4 (SDA) - Connect to ESP32's PIN 21 with a 10K (100MHz I2C Bus speed) pull-up resistor
    PIN 5 (SCL) - Connect to ESP32's PIN 22 with a 10K (100MHz I2C Bus speed) pull-up resistor
    DECOUPING:    Minimal decoupling consists of a 0.1uF Ceramic Capacitor between the VCC & GND PINS. For improved performance,
                    add a 1uF and a 10uF Ceramic Capacitors as well across these pins
    I2C ADDRESSES
    -------------
    Each MCP3221 has 1 of 8 possible I2C addresses (factory hardwired & recognized by its specific part number & top marking
    on the package itself):

        PART                  DEVICE I2C ADDRESS          PART
        NUMBER             (BIN)      (HEX)     (DEC)     MARKING
    MCP3221A5T-E/OT       01001101      0x4D       77       GA

    source: https://github.com/nadavmatalon/MCP3221
*/

//Here is the pump current code
float getRawData(){
  float rawData = 0;
  float rawDataSum = 0;
  for(int i=0; i<35; i++){
    rawData = analogRead(current_sensor_pin);
    rawDataSum = rawData + rawDataSum;
  }

  return rawDataSum / 35;
}

void loggerTask(void *logger_params)
{
    // delay(10000);
    Serial.println("LOGGER");

    configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

    // NVSInterface::eraseAllEventLogsPayload();
    unsigned long system_status_time_ref{0}; // Timer reference for the system status event
    // Task event parameter
    uint32_t ulEventValue;
    // Task bits to clear
    uint32_t ulPendingEventsToClear = 0;
    // Task event notification receiver
    BaseType_t xEventOccured;

    // Getting data from main task parameter
    GenericData_t data = *(GenericData_t *)  logger_params;
    //global_firmware_version = data.firmwareVersion; // to keep track of JSON version in time while on
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Problem: Watchdog got triggered when the new binary is being downloaded because it takes
    //           too much time to end and it thinks is an infinity loop.
    //
    //  Solution: Disable the watchdog for this task (partially)
    //
    //  Testing: TRUE. Tests has been made letting the device running for a period of an hour 
    //           without being reseted.
    //
    //  A more elegant solution to this would be to work with another task trying to feed the 
    //  watchdog before it got triggered. However, it seems the watchdog looks over every task and
    //  if any of them if having more processing time than expected it will reset the device.
    //  The code will be save here to be tested deeper and improve the system functionality
    //  without having to disable the watchdog.
    //
    // #include "soc/rtc_wdt.h"
    // rtc_wdt_protect_off();
    // rtc_wdt_disable();
    // // create a lowest priority FreeRTOS task that just loops and calls the feed method 
    // // with a delay less then the timeout, giving enough time for higher priority 
    // // "business logic" to run. Example:
    // rtc_wdt_set_length_of_reset_signal(RTC_WDT_SYS_RESET_SIG, RTC_WDT_LENGTH_3_2us);
    // rtc_wdt_set_stage(RTC_WDT_STAGE0, RTC_WDT_STAGE_ACTION_RESET_SYSTEM);
    // rtc_wdt_set_time(RTC_WDT_STAGE0, 250);
    // while(true) {
    //     rtc_wdt_feed();
    //     vTaskDelay(pdMS_TO_TICKS(100));
    // }
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////
/*
    // To disable the watchdog
    rtc_wdt_protect_off();
    rtc_wdt_disable(); 
    disableCore0WDT();
    disableLoopWDT();   
    esp_task_wdt_delete(NULL);

    //ADC Configuration
    analogReadResolution(adc_resolution);
    analogSetAttenuation(ADC_11db);
    // Backup analog pin register
    backup_pin_register = READ_PERI_REG(SENS_SAR_READ_CTRL2_REG);
    backup_pin_register_battery = READ_PERI_REG(SENS_SAR_READ_CTRL2_REG);
    //We have to do the 2 previous instructions BEFORE EVERY analogRead() calling!  */

    xCurrentQueue = xQueueCreate(1, sizeof(&raw_data));
    if (xCurrentQueue == nullptr)
    {
        //Serial.println("Error: Current Queue could not be created");
        //Serial.println("Rebooting...");
        esp_restart();
    }
  //Peristaltic pump init
//  pinMode(magnetic_switch, INPUT);    

    while (true)
    {  
      float case_temperature = readCaseTemperature();
        // An event occurred?
        if (xEventOccured == pdPASS)
        {
            // Is it a system status event?
            if ((ulEventValue & logger_task_system_status_bit) != 0)
            {
                //Serial.println("***LOGGER***Sending pump current data to xCurrentQueue...");
                raw_data = getRawData();
                unsigned int *xPointerCurrentLevel = &raw_data;
                //Serial.println(*xPointerCurrenLevel);
                if (xQueueSend(xCurrentQueue, (void*) &xPointerCurrentLevel, (TickType_t) 0) != pdPASS)
                {
                  Serial.println("Failed to write to xCurrentQueue");
                }
            }
        }      
        xEventOccured = xTaskNotifyWait(pdFALSE, logger_task_system_status_bit, &ulEventValue, pdMS_TO_TICKS(1UL));
    //Sending the current data of the pumpnin amps
    //float pump_current = sendCurrentData();
    //Variable to store how much stack is consuming this task
    //UBaseType_t uxHighWaterMark;
   /* if (WiFi.status() == WL_CONNECTED) {
    static bool flag = false;
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo)) {
      Serial.println("Failed to obtain time");
      return;
    }
    char timeStringBuff[50]; //50 chars should be enough
    strftime(timeStringBuff, sizeof(timeStringBuff), "%A, %B %d %Y %H:%M:%S", &timeinfo);
    String asString(timeStringBuff);
    asString.replace(" ", "-");
    Serial.print("Time:");
    Serial.println(asString);
    Serial.println(raw_data);
    String urlFinal = "https://script.google.com/macros/s/"+GOOGLE_SCRIPT_ID+"/exec?"+"date=" + asString + "&sensor=" + case_temperature;
    Serial.print("POST data to spreadsheet:");
    Serial.println(urlFinal);
    HTTPClient http;
    http.begin(urlFinal.c_str());
    http.setFollowRedirects(HTTPC_STRICT_FOLLOW_REDIRECTS);
    int httpCode = http.GET(); 
    Serial.print("HTTP Status Code: ");
    Serial.println(httpCode);
    //---------------------------------------------------------------------
    //getting response from google sheet
    String payload;
    if (httpCode > 0) {
        payload = http.getString();
        Serial.println("Payload: "+payload);    
    }
    //---------------------------------------------------------------------
    http.end();
    }  */

        if (millis() - system_status_time_ref >= System_status_interval)
        {
            // Prints available heap
            ESP_LOGD(__func__, "FREE HEAP: %zu", heap_caps_get_free_size(MALLOC_CAP_DEFAULT));
            // Sending the System Status event report...
           // Serial.println("Retrieving tasks sensors information to be sent in UART...");
            // Notify Battery task about the event, so this task can receive the battery level data by reading a queue
            xTaskNotify(xFiltersTaskHandle, filters_task_pump_status_bit, eSetBits);
            // Notify Battery task about the event, so this task can receive the battery level data by reading a queue
            xTaskNotify(xBatteryTaskHandle, battery_task_system_status_bit, eSetBits);
            // Notify Battery task about the event, so this task can receive the battery level data by reading a queue
            //xTaskNotify(xBatteryTaskHandle, battery_charge_task_system_status_bit, eSetBits);
            // Notify Water Top Off task about the event, so this task can receive the case temperature data by reading another a queue
            xTaskNotify(xWTOTaskHandle, wto_task_system_status_bit, eSetBits);
            // Notify Water Top Off task about the event, so this task can receive the dirty water level status data by reading another a queue
            xTaskNotify(xPumpTaskHandle, dirty_water_pump_status_bit, eSetBits);
            
            bool *xPointerDirtyWaterStatusLevel, *xPointerChargerStatusLevel = 0;
            // Define the pointer of the battery and case temperature data (Pointers addresses provided by two Queues to read from)
            float *xPointerBatteryLevel, *xPointerFilterStatusLevel, *xPointerCaseTemperature;
            // Wait for the xFilterStatusQueue to be available
            BaseType_t xFilterStatusAvailable = xQueueReceive(xFilterStatusQueue, &(xPointerFilterStatusLevel), Queue_current_wait_time);
            // Wait for the xBatteryQueue to be available
            BaseType_t xBatteryAvailable = xQueueReceive(xBatteryQueue, &(xPointerBatteryLevel), Queue_current_wait_time);
            // Wait for the xBatteryQueue2 to be available
            //BaseType_t xBatteryChargerAvailable = xQueueReceive(xBatteryQueue2, &(xPointerChargerStatusLevel), Queue_current_wait_time);
            // Wait for the xCaseTemperatureQueue to be available
            BaseType_t xCaseTemperatureAvailable = xQueueReceive(xCaseTemperatureQueue, &(xPointerCaseTemperature), Queue_current_wait_time);
            // Wait for the xdirtyWaterQueue to be available
            BaseType_t xDirtyWaterQueueAvailable = xQueueReceive(xdirtyWaterQueue, &(xPointerDirtyWaterStatusLevel), Queue_current_wait_time);
            //&& xFilterStatusAvailable == pdPASS
            //ESP_LOGE(__func__, "***LOGGER--> going to evaluate availability***");
            if(xBatteryAvailable != pdPASS)
            {
                //Serial.println("***LOGGER***Failed to read from xBatteryQueue ");
            }
            else if(xCaseTemperatureAvailable != pdPASS)
            {
                //Serial.println("***LOGGER***Failed to read from xCaseTemperatureQueue");
            }
            else if(xFilterStatusAvailable != pdPASS)
            {
               // Serial.println("***LOGGER*** Failed to read from xFilterStatusQueue");
                ESP_LOGE(__func__, "Filter status received: %i", *xPointerFilterStatusLevel);
            }
            else if(xDirtyWaterQueueAvailable != pdPASS)
            {
                //Serial.println("***LOGGER*** Failed to read from xDirtyWaterQueueAvailable");
            }
            else
            {
                ESP_LOGE(__func__, "Battery level received: %.2f%%", *xPointerBatteryLevel);
                ESP_LOGE(__func__, "Case's temperature received: %.2fC", *xPointerCaseTemperature);
                ESP_LOGE(__func__, "Filter status received: %i", *xPointerFilterStatusLevel);
                ESP_LOGE(__func__, "Dirty Water level status received: %i", *xPointerDirtyWaterStatusLevel);
                //ESP_LOGE(__func__, "Battery Charger Status: %i", *xPointerChargerStatusLevel);

                //    The Raspberry Pi seems to have problems working alongside cJSON library and it doesnot understand the format.
                //    When sending payload, a workaround was made using Arduino JSON     
            }            
            system_status_time_ref = millis();
        }
        
        // ESP_LOGD(__func__, "Event value before = %i", ulEventValue);
        xTaskNotifyWait(ulPendingEventsToClear, pdFALSE, &ulEventValue, pdMS_TO_TICKS(1UL));
        ulPendingEventsToClear = 0; // Clear pending events variable
        // ESP_LOGD(__func__, "Event value after = %i", ulEventValue);    
    
        // Is it a Flush event?
        if ((ulEventValue & logger_task_flush_bit) != 0)
        {
            // Prints available heap
            ESP_LOGD(__func__, "FREE HEAP: %zu", heap_caps_get_free_size(MALLOC_CAP_DEFAULT));
            ESP_LOGD(__func__, "****************** FLUUUUUSSSSSSHHHHHHHHHH");
            ESP_LOGD(__func__, "A Flush event has occurred");
            // Notifying Filters task about a new flush event
            xTaskNotify(xFiltersTaskHandle, filters_task_flush_bit, eSetBits);
            // Define the pointer of the structure where it will point to (Pointer provided by Queue)
            std::unordered_map<int, float> *xPointerPressureValues;
            // Wait for the xPressureValuesQueue to be available
            if (xQueueReceive(xPressureValuesQueue, &(xPointerPressureValues), Queue_current_wait_time) == pdPASS)
            {
                // Iterator
                std::vector<float> filters_values;
                for (auto &element : *xPointerPressureValues) 
                {
                    filters_values.push_back(element.second);
                    ESP_LOGE(__func__, "Pressure sensor %i value received = %.3f", element.first, element.second);
                }
                

                String jsonString2 ="{\"filters_pressure\":";
                jsonString2 += "{\"f1\":";
                jsonString2 += filters_values[0];
                jsonString2 +=",\"f2\":";
                jsonString2 += filters_values[1];
                jsonString2 +=",\"f3\":";
                jsonString2 += filters_values[2];
                jsonString2 +=",\"f4\":";
                jsonString2 += filters_values[3];
                jsonString2 +="}";

                Serial2.println(jsonString2);
                // Serial.println(jsonString2);
            }
            else
            {
               // Serial.println("Failed to read from xPressureValuesQueue");     
            }

            ulPendingEventsToClear = ulPendingEventsToClear | logger_task_flush_bit;
        }

        vTaskDelay(logger_task_delay_ms);
    }
}

void DATALOGGER::begin(GenericData_t data_param)
{
    // WiFi routine task
    xLOGGERTaskStatus = xTaskCreatePinnedToCore(
        &loggerTask,            // Pointer to task
        "loggerTask",           // Name of the task
        logger_stack_size,      // Task stack size
        (void*)&data_param,      // Parameters to the task
        3,                   // Priority
        &xLOGGERTaskHandle,     // Handler of the task
        ARDUINO_RUNNING_CORE // Core ID
    );
    if (xLOGGERTaskStatus == pdPASS)
    {
        //Serial.println("Data Logger Task initiated");
    }
    else
    {
        //Serial.println("An error occurred while creating the Data Logger task");
    }
}