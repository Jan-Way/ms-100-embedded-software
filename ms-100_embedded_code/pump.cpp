//===============================================================================================================================================
/*
Project name: MS-100
File name: pump.cpp
Description: 
*/
//===============================================================================================================================================

#include "Arduino.h"
#include "pump.h"
#include "logger.h"
#include "filters.h"
#include "Wire.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

// #include <Wire.h>
// #include "includes/external/includes/MCP3221.h"

BaseType_t xPumpTaskStatus;
TaskHandle_t xPumpTaskHandle;
QueueHandle_t xdirtyWaterQueue;

unsigned long turn_on_pump_ref{0};  // Timer reference to wait before turning on the pump
unsigned long turn_off_pump_ref{0}; // Timer reference to wait before turning off the pump
unsigned long empty_reservoir_ref{0}; // Timer reference to wait before turning off the pump

bool turn_on_pump_flag{false};  // Status flag related to turn on pump counter
bool turn_off_pump_flag{false}; // Status flag related to turn off pump counter
bool empty_reservoir_flag{false};  // Status flag related to turn on pump counter

bool amps_threshold{false};     // Status flag to know if the maximun permited current that flows through the pump has been reached or not

bool filterstatus = false;

bool dirty_water_reservoir_status = false;

unsigned int raw_current_data{0};
double current_sensor{0};
double voltage_level{0};

bool readLevelSensor(int sensor_pin)
{
    for (int i = 0; i < 6; ++i) 
    {
      delay(50);
    }
    // ESP_LOGD(__func__, "Performing a digital read from PUMP TASK --> PIN: %i", sensor_pin);
    return digitalRead(sensor_pin);
}

// Module: Pump logic
void pumpTask(void *pump_params)
{
    Serial.println("Pump");

    // Setting up task I/Os
    pinMode(pump_float_sensor, INPUT);
    pinMode(dirty_float_sensor, INPUT);
    pinMode(pump, OUTPUT);
    //pinMode(fan, OUTPUT);

    uint32_t ulEventValue;
    // Task bits to clear
    uint32_t ulPendingEventsToClear = 0;
    // Task event notification receiver
    BaseType_t xEventOccured;

    xdirtyWaterQueue = xQueueCreate(1, sizeof(&dirty_water_reservoir_status));
    if (xdirtyWaterQueue == nullptr)
    {
        //Serial.println("Error: dirty_water_reservoir_status Queue could not be created");
        //Serial.println("Rebooting...");
        esp_restart();
    }

    while (true)
    {
        ///////////////////////////////////////////////////////////////////////////////////////////
        //
        //      MCR1101 --> Sensor de Corriente
        //      MCP3221 --> ADC Externo
        //
        //      NOMINAL TRANSFER FUNCTION
        //      Vout = VCC/2 + IIN x 66mV/A x VCC/3.3V
        // 
        ///////////////////////////////////////////////////////////////////////////////////////////
  
        // Notify LOGGER task about the event, so this task can receive the current level data by reading a queue
        xTaskNotify(xLOGGERTaskHandle, logger_task_system_status_bit, eSetBits);
        // Define the pointer of the current
        unsigned int *xPointerPumpCurrentLevel;
        // Wait for the xCurrentQueue to be available
        BaseType_t xCurrentAvailable = xQueueReceive(xCurrentQueue, &(xPointerPumpCurrentLevel), Queue_current_wait_time);
        //Serial.println(*xPointerPumpCurrenLevel);        
        
        if (xCurrentAvailable == pdPASS)
        {           
            raw_current_data = *xPointerPumpCurrentLevel;
            //Serial.println(raw_current_data);
            voltage_level = raw_current_data*3.3/3900;
            //Serial.println(voltage_level);
            //ESP_LOGE(__func__, "Pump current sensor voltage level: %f", voltage_level );  
            current_sensor = ( voltage_level - 1.65 )/0.066;
            //Serial.println(current_sensor);
            //ESP_LOGE(__func__, "Pump current: %f", current_sensor );  

            
            if(current_sensor < 0)
            {
                current_sensor = 0;
            }
            else if(current_sensor > 20)
            {
                current_sensor = 20;
            }
            if (current_sensor >= max_current_sensor)
            { 
                // If the current that flows through the pump is greater than 5.5A, must activate the flag
                amps_threshold = true;
            }
            else
            {
                amps_threshold = false;
            }
            

            
            // // Notify Battery task about the event, so this task can receive the battery level data by reading a queue
            // xTaskNotify(xFiltersTaskHandle, filters_task_pump_status_bit, eSetBits);
            // // Define the pointer of the battery and solenoid temperature data (Pointers addresses provided by two Queues to read from)
            // bool *xPointerFilterStatusLevel;
            // // Wait for the xFilterStatusQueue to be available
            // BaseType_t xFilterStatusAvailable = xQueueReceive(xFilterStatusQueue, &(xPointerFilterStatusLevel), Queue_current_wait_time);

            // if (xFilterStatusAvailable == pdPASS)
            // {
            //     filterstatus = *xPointerFilterStatusLevel;
            // } 
            // else if(xFilterStatusAvailable != pdPASS)
            // {
            //     ESP_LOGE(__func__, "***PUMP task*** xFilterStatusAvailable not available");
            // }

            if (!amps_threshold && !filterstatus)
            { 
                // If the current that flows through the pump is lower than 8A, must follow the logic
                // High  = not water down there
                if (!readLevelSensor(dirty_float_sensor))
                {
                    empty_reservoir_flag = false;
                    if (readLevelSensor(pump_float_sensor))
                    {
                        dirty_water_reservoir_status = false;
                        turn_off_pump_flag = false;
                        //If both sensors are open, it means that it has no water
                        // Then, if there is no water, and the water exit flag has not been
                        // activated, then counting begins. The counter allows you to create
                        // a time space in which you can take samples and compare whether the
                        // water is really low or just rinsing the mop in the nozzle, so the
                        // water is shaking.
                        if (!turn_on_pump_flag)
                        {
                            turn_on_pump_ref = millis();
                            turn_on_pump_flag = true;
                        }

                        if (millis() - turn_on_pump_ref >= turn_on_pump_interval)
                        {
                            // If, indeed, the water is low,
                            // then the two sensors will be below the mark for more than a set time.
                            // Then, the pump must be activated.
                            ESP_LOGD(__func__, "Pump turned ON");
                            digitalWrite(pump, HIGH);
                            //digitalWrite(fan, HIGH);
                            turn_on_pump_flag = false;
                        }
                    }
                    else if (!readLevelSensor(pump_float_sensor))
                    {
                        turn_on_pump_flag = false;
                        // In any other situation, the bomb must be deactivated.
                        // If there is not dirty water, the filter system can not be activated
                        if (!turn_off_pump_flag)
                        {
                            turn_off_pump_flag = true;
                            turn_off_pump_ref = millis();
                        }

                        //if (turn_off_pump_flag)
                        //{
                            if (millis() - turn_off_pump_ref >= turn_off_pump_interval)
                            {
                                ESP_LOGD(__func__, "Pump turned OFF");
                                digitalWrite(pump, LOW);
                                //digitalWrite(fan, LOW);
                                turn_off_pump_flag = false;
                            }
                        //}
                    }
                    else
                    {
                        //ESP_LOGD(__func__, "Not passing through readLevelSensor(pump_float_sensor)");
                    }
                }
                else
                {
                    // ESP_LOGD(__func__, "Not passing through readLevelSensor(dirty_float_sensor)");
                    turn_on_pump_flag = false;
                    if (!empty_reservoir_flag)
                    {
                        empty_reservoir_ref = millis();
                        empty_reservoir_flag = true;
                        delay(20);
                    }
                    if (millis() - empty_reservoir_ref >= empty_reservoir_interval)
                    {
                        // If, indeed, the water is low,
                        // then the two sensors will be below the mark for more than a set time.
                        // Then, A person will need to re-fill a resovoir tank.
                        ESP_LOGD(__func__, "Reservoir tank is empty! ");
                        digitalWrite(pump, LOW);
                        //digitalWrite(fan, LOW);
                        dirty_water_reservoir_status = true;
                        empty_reservoir_flag = false;
                    }
                }
                xEventOccured = xTaskNotifyWait(pdFALSE, dirty_water_pump_status_bit, &ulEventValue, pdMS_TO_TICKS(1UL));
                // An event occurred?
                if (xEventOccured == pdPASS)
                {
                    // Is it a system status event?
                    if ((ulEventValue & dirty_water_pump_status_bit) != 0)
                    {
                      //  Serial.println("***LOGGER*** Sending dirty water reservoir status data to xdirtyWaterQueue...");
                        bool *xPointerDirtyWaterStatus = &dirty_water_reservoir_status;
                        if (xQueueSendToBack(xdirtyWaterQueue, (void*) &xPointerDirtyWaterStatus, (TickType_t) 0) != pdPASS)
                        {
                         //   Serial.println("Failed to write to xdirtyWaterQueue");
                        }
                    }
                }
            }
            else
            {
                digitalWrite(pump, LOW);
                //digitalWrite(fan, LOW);
                ESP_LOGD(__func__, "Emergency Pump turned OFF, it exceed the maximum threshold");
                turn_on_pump_flag = false;
                turn_off_pump_flag = false;
            }
        }
        vTaskDelay(pump_task_delay_ms);
    }
}